<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
// const SEND_EMAIL = 'info@autogarantcity.ru';
const SEND_EMAIL = 'callcenter@autogarantcity.ru';
const SECRET_CAPTCHA_KEY_GOOGLE = '6LcTwBMlAAAAAPx4DXQrxAdyHr3BsJrK_PYb-0TU';
const IMAGES_URL = 'https://autogarantcity.ru';
$httpX = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : false;
/*if (!isset($_REQUEST['method']) || $httpX == false) {
    header("HTTP/1.1 404 Not Found");
    echo 'Ops :(' . (!isset($_REQUEST['method'])) . ($httpX == false);
    exit;
}*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

function collapseArray($arr1, $arr2)
{
    $keys = array_unique(array_merge(array_keys($arr1), array_keys($arr2)));
    $result = [];
    foreach ($keys as $key) {

        if (isset($arr1[$key]) && isset($arr2[$key]) && $arr1[$key] === $arr2[$key]) {
            $result[$key] = $arr1[$key];
        }
        if (isset($arr1[$key]) && isset($arr2[$key]) && $arr1[$key] !== $arr2[$key]) {
            $result[$key] = is_array($arr1[$key]) ? array_merge($arr1[$key], [$arr2[$key]]) : [$arr1[$key], $arr2[$key]];
        }
        if (!isset($arr1[$key]) || !isset($arr2[$key])) {
            $result[$key] = isset($arr1[$key]) ? $arr1[$key] : $arr2[$key];
        }
    }
    return $result;
}

function normalizeItems($items)
{
    $items_ = $items;
    $itemsByIds = [];
    foreach ($items_ as $item) {
        if (isset($itemsByIds[$item['ID']])) {
            $itemsByIds[$item['ID']] = collapseArray($itemsByIds[$item['ID']], $item);
        } else {
            $itemsByIds[$item['ID']] = $item;
        }
    }

    foreach ($itemsByIds as &$item) {
        $gallery = [];
        $item = ['id' => $item['ID'], 'name' => $item['NAME'],
            'iblock_section_id' => $item['IBLOCK_SECTION_ID'],
            'preview_text' => $item['PREVIEW_TEXT'],
            'preview_picture' => CFile::GetPath($item['PREVIEW_PICTURE']),
            'detail_picture' => CFile::GetPath($item['DETAIL_PICTURE']),
            'gallery' => $gallery, 'other' => $item];

    }

    return $itemsByIds;
}

function normalizeServiceCards($items)
{
    $items_ = $items;
    $itemsByIds = [];
    foreach ($items_ as $item) {
        if (isset($itemsByIds[$item['ID']])) {
            $itemsByIds[$item['ID']] = collapseArray($itemsByIds[$item['ID']], $item);
        } else {
            $itemsByIds[$item['ID']] = $item;
        }
    }

    foreach ($itemsByIds as &$item) {
        $gallery = [];
        $gifId = isset($item['PROPERTY_GIFT_VALUE']) ? $item['PROPERTY_GIFT_VALUE'] : null;
        $url = isset($item['PROPERTY_URL_VALUE']) ? $item['PROPERTY_URL_VALUE'] : null;
        $item = ['id' => $item['ID'], 'title' => $item['NAME'],
            'image' => IMAGES_URL . CFile::GetPath($item['DETAIL_PICTURE']),
            'gallery' => $gallery,
            'url' => $url,
            'other' => $item,
        ];
        if ($gifId != null) {
            $arFilter = array("ID" => $gifId);
            $res = CIBlockElement::GetList(array(), $arFilter); // с помощью метода CIBlockElement::GetList вытаскиваем все значения из нужного элемента
            if ($rawGift = $res->GetNextElement()) {
                ; // переходим к след элементу, если такой есть
                $gift = normalizeGift($rawGift);// $ob->GetProperties(); // свойства элемента
                $item['gift'] = $gift;
            } else {
                $item['gift'] = null;
            }

        }
    }

    return $itemsByIds;
}

function normalizeGift($rawGift)
{
    $result = [];
    $properties = $rawGift->getProperties();
    foreach ($properties as $key => $value) {
        $result[$key] = $value['VALUE'];
    }
    $fields = $rawGift->getFields();
    $result['title'] = $fields['NAME'];
    if (isset($fields['DETAIL_PICTURE'])) {
        $result['image'] = IMAGES_URL . CFile::GetPath($fields['DETAIL_PICTURE']);
    }
    $result['fields'] = $fields;
    return $result;
}

function getReviews()
{
    CModule::IncludeModule("iblock");

    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => 40,
        'GLOBAL_ACTIVE' => 'Y',
        'INCLUDE_SUBSECTIONS' => 'Y'
    );

    $arSelect = array('IBLOCK_ID', 'ID', 'NAME', 'IBLOCK_SECTION_ID', 'DESCRIPTION');
    $arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');
    $rsItems = CIBlockElement::GetList([], $arFilter, false, array("nPageSize" => 50), ['IBLOCK_ID', 'IBLOCK_SECTION_NAME', 'ID', 'NAME',
        'ACTIVE', 'IBLOCK_SECTION_ID', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_PICTURE']);
    $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);

    $itemsByIds = normalizeItems($rsItems->arResult);

    $sectionList = [];
    while ($arSection = $rsSections->GetNext()) {
        $sectionList[$arSection['ID']] = [
            'id' => $arSection['ID'],
            'name' => $arSection['NAME']
        ];
    }

    foreach ($itemsByIds as &$item) {
        $item['section'] = $sectionList[$item['iblock_section_id']];
    }
    unset($item);

    return array_values($itemsByIds);
}

function getServiceCards()
{
    CModule::IncludeModule("iblock");

    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => 41,
        'GLOBAL_ACTIVE' => 'Y',
        'INCLUDE_SUBSECTIONS' => 'N'
    );

    $rsItems = CIBlockElement::GetList([], $arFilter, false, array("nPageSize" => 50), ['IBLOCK_ID', 'IBLOCK_SECTION_NAME', 'ID', 'NAME',
        'ACTIVE', 'IBLOCK_SECTION_ID', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', "PROPERTY_GIFT", "PROPERTY_URL"]);
    // 1864PROPERTY_GIFT_VALUE

    $itemsByIds = normalizeServiceCards($rsItems->arResult);

    return array_values($itemsByIds);
}

function sendMail()
{
    $result = false;
    $json = file_get_contents('php://input');
    $data = json_decode($json, true);
    $_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . SECRET_CAPTCHA_KEY_GOOGLE . "&response={$data['token']}");
    $captchaData = json_decode($_response, true);
    if (isset($captchaData['success']) && $captchaData['success'] == true) {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        if ($data['name'] !== 'test') {
            mail(SEND_EMAIL, 'Заявка на консультацию',
                "
<div>Заявка на консультацию с мобильного сайта autogarantcity.ru</div>
<div>Указанное имя: {$data['name']}</div>
<div>Указанный телефон: {$data['phone']}</div>
", $headers);
        }
        $result = true;
    }
    return $result;
}


switch ($_REQUEST['method']) {
    case 'reviews':
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode(getReviews());
        break;
    case 'service_cards':
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode(getServiceCards());
        break;
    case 'sendMail':
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode(sendMail());
        break;
    default:
        echo json_encode(array('error_msg' => 'Ops :('));
}