import React from "react";
import classes from "./plug.module.scss";
import Button from "../../components/Button";
import {ButtonStyles} from "../../components/Button/Button";
import {useNavigate} from "react-router-dom";

const Plug = () => {
    const navigate = useNavigate();
    return (
        <div className={classes.plug}>
            <div className={classes.back} onClick={() => {
                navigate(-1);
            }}>назад</div>
            <div className={classes.logo}/>
            <div className={classes.text}>Страница находится<br/>в разработке</div>
            <div className={classes.buttons}>
                <Button handler={() => {
                    window.location.href = 'https://ortus.ru/w/2222999';
                }} styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}>Записаться</Button>
                <Button handler={() => null} styles={[ButtonStyles.blueBorder, ButtonStyles.fullWidth]}>Задать вопрос</Button>
            </div>
            <div className={classes.phone}>+7(423)2222-999</div>
            <div className={classes.greeting}>Мы рады вам 7 дней в неделю</div>
        </div>
    );
}

export default Plug;