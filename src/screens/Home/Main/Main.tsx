import React from "react";
import classes from './main.module.scss'
import Button from "components/Button";
import {Screens} from "types/global";

const Main = ({changeScreen}: { changeScreen: (screen: Screens) => void }) => {
    return <div className={classes.screen}>
        <div className={classes.greeting}>Добро пожаловать</div>
        <div className={classes.introduce}>Меня зовут Валерия</div>
        <div className={classes.introduce}>Что вас интересует?</div>
        <div className={classes.buttons}>
            <Button handler={() => changeScreen(Screens.contacts)}>Контакты</Button>
            <Button handler={() => {
                window.location.href = 'https://ortus.ru/w/2222999';
            }}>Записаться</Button>
            <Button handler={() => changeScreen(Screens.acquaint)} large>Познакомиться с компанией</Button>
            <Button handler={() => changeScreen(Screens.contacts)} large>Порекомендовать другу</Button>
        </div>
    </div>;
}
export default Main;