import React, { useCallback, useEffect, useState } from "react";
import Header from "../../components/Header";
import Main from "./Main";
import Contacts from "./Contacts";
import Acquaint from "./Acquaint";
import classes from './styles.module.scss'
import Footer from "../../components/Footer";
import { presentScreens } from "../../demoData/presentScreens/presentScreens";
import { Screens } from "types/global";

interface HomeProps {
    startStatus: boolean;
    changeStartStatus: (status: boolean) => void;
}

const Home: React.FC<HomeProps> = ({ startStatus, changeStartStatus }) => {
    const [isScrolled, setIsScrolled] = useState(false);
    const [showArrowToTop, setShowArrowToTop] = useState(false);
    const [showFooter, setShowFooter] = useState(false);
    const [screen, setScreen] = useState(Screens.main);

    const preventDefault = useCallback((e: TouchEvent) => {
        if (startStatus) {
            e.preventDefault();
        }
    }, [startStatus]);

    useEffect(() => {
        if (startStatus) {
            document.body.style.position = "fixed";
            document.body.style.overflow = "hidden";
        } else {
            document.body.style.position = "relative";
            document.body.style.overflow = "auto";
        }
    }, [startStatus]);

    useEffect(() => {
        document.body.addEventListener('touchmove', preventDefault, { passive: false });
        return () => {
            document.body.removeEventListener('touchmove', preventDefault);
        };
    }, [preventDefault]);

    useEffect(() => {
        const handleScroll = (e: Event) => {
            if (!startStatus) {
                const target = e.target as Document;
                const scrollValue = target.documentElement.scrollTop;
                setIsScrolled(scrollValue > 20);
                setShowFooter(scrollValue > 0.8 * target.documentElement.clientHeight);
                setShowArrowToTop(scrollValue > 1.4 * target.documentElement.clientHeight);
            }
        };
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [startStatus]);

    useEffect(() => {
        const element = document.getElementById(screen);
        if (element && !startStatus) {
            element.scrollIntoView({ behavior: 'smooth' });
        }
    }, [screen, startStatus]);

    const scrollToTop = () => {
        const element = document.getElementById(Screens.main);
        if (element && !startStatus) {
            element.scrollIntoView({ behavior: 'smooth' });
        }
    }

    return (
        <div className={classes.scrollingArea + ' ' + (startStatus ? classes.scrollLocked : '')}>
            <Header
                onSecondScreen={isScrolled}
                logoClickHandler={() => {
                    if (screen !== Screens.main) {
                        setScreen(Screens.main);
                    } else {
                        scrollToTop();
                    }
                }}
                changeScreen={(screen: Screens) => {
                    changeStartStatus(false);
                    setScreen(screen);
                }}
            />
            <div id={Screens.main} />
            <Main changeScreen={(screen) => {
                changeStartStatus(false);
                setScreen(screen);
            }} />
            <Acquaint presents={presentScreens} />
            <div id={Screens.contacts} />
            <Contacts />
            <div
                className={classes.arrow + ' ' + (showArrowToTop ? classes.visible : '')}
                onClick={() => {
                    if (screen !== Screens.main) {
                        setScreen(Screens.main);
                    } else {
                        scrollToTop();
                    }
                }}
            />
            <Footer active={showFooter} />
        </div>
    );
}

export default Home;
