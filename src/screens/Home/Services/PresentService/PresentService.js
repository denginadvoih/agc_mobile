import React from "react";
import PropTypes from 'prop-types';
import 'swiper/css';
import classes from './presentService.module.scss'
import Button from "components/Button";
import {ButtonStyles} from "components/Button/Button";
import {useNavigate} from "react-router-dom";
import {ROUTES} from "../../../../routes";

const PresentService = ({id, title, image, gift, url}) => {
    const navigate = useNavigate();
    const outerNavigate = (url) => {
        window.location.href = url;
    }
    //TODO сделать подарок из админки (просто переход на внешний урл)
    return (
        <div className={classes.card} onClick={() => url && outerNavigate(url)}>
            <div className={classes.image}>
                <img src={image} alt={''}/>
            </div>
            <div className={classes.title}>{title}</div>
            {(gift || null) !== null
                ? <div className={classes.gift} onClick={(e) => {
                    e.stopPropagation();
                    //navigate(ROUTES.serviceGift(id));
                    outerNavigate('https://all.autogarantcity.ru/?utm_source=saitmobileags&utm_campaign=bannerzamena');
                }}/>
            : null
            }
            <Button styles={[ButtonStyles.blue, ButtonStyles.small]} handler={() => navigate(ROUTES.readOn)}>Подробно</Button>
        </div>
    );
}

PresentService.propTypes = {
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    title: PropTypes.string,
    description: PropTypes.string,
    angaraId: PropTypes.number

}

export default PresentService;