import {connect} from "react-redux";
import { fetchServiceCards } from "redux/slices/dataSlice";
import Gift from './Gift';
import {ServiceCard} from "../../../../../types/global";

const mapStateToProps = (state: {data: {serviceCards: ServiceCard[]}}) => {
    return {
        serviceCards: state.data.serviceCards
    }
};

export default connect(mapStateToProps, { fetchServiceCards })(Gift);