import React, {useEffect} from "react";
import classes from './gift.module.scss';
import {useParams} from "react-router-dom";
import {ButtonStyles} from "../../../../../components/Button/Button";
import Button from "../../../../../components/Button";
import Close from "../../../../../components/Close";
import {useNavigate} from "react-router-dom";
import {ServiceCard} from "../../../../../types/global";

const Gift = ({serviceCards, fetchServiceCards}: {serviceCards: ServiceCard[]; fetchServiceCards: () => void}) => {
    const params = useParams();
    const navigate = useNavigate();
    useEffect(() => {
        const serviceCardId = parseInt(params.serviceCardId  || '');
        if (!(serviceCards || []).some(s => s.id === serviceCardId)) {
            fetchServiceCards();
        }
    }, [fetchServiceCards, params, serviceCards]);

    const serviceCardId = parseInt(params.serviceCardId || '');
    const serviceCard = (serviceCards || []).find(s => s.id === serviceCardId);
    const gift: Record<string, string> = (serviceCard || {}).gift || {};

    return (
        <div className={classes.screen}>
            <Close className={classes.close} handler={() => navigate(-1)}/>
            <div className={classes.title}>Подарки в честь знакомства</div>
            {[1, 2, 3, 4].map(blockNumber =>
                <div className={classes.block} key={blockNumber}>
                    {[1, 2, 3].map(textNumber => {
                            return <div
                                className={classes['text' + textNumber]}>{gift?.[`f_${blockNumber}_${textNumber}`]}</div>;
                        }
                    )}

                </div>
            )}
            <div className={classes.block4}>
                Cравни цены и сервис, почувствуй разницу
            </div>
            <div className={classes.button}>
                <Button handler={() => {
                    window.location.href = 'https://ortus.ru/w/2222999';
                }} styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}>Записаться</Button>
            </div>
            <div className={classes.buttons}>
                <a className={classes.whatsapp} href="https://wa.me/79140773596">Написать</a>
                <a className={classes.phone} href="tel:+74232222999">Позвонить</a>
            </div>
        </div>
    );
}

export default Gift;