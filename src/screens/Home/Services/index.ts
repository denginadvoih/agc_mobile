import {connect} from "react-redux";
import { fetchServiceCards } from "redux/slices/dataSlice";
import Services from './Services';
import {ServiceCard} from "../../../types/global";

const mapStateToProps = (state: {data: { serviceCards: ServiceCard[] }}) => {
    return {
        serviceCards: state.data.serviceCards
    }
};

export default connect(mapStateToProps, { fetchServiceCards })(Services);