import React, {useEffect} from "react";
import classes from './services.module.scss'
import PresentService from "./PresentService";
import Catalogues from "../Catalogues";
import {ServiceCard} from "../../../types/global";

const Services = ({ serviceCards, fetchServiceCards }: {serviceCards: ServiceCard[]; fetchServiceCards: () => void}) => {

    useEffect(() => {
        fetchServiceCards();
    }, [fetchServiceCards]);

    return <div className={classes.screen}>
        <div className={classes.header}>
            <div>Давайте знакомиться!</div>
            <div>Выберите желаемую услугу и почувствуйте разницу</div>
        </div>
        <div className={classes.services}>
            {(serviceCards || []).map(s => <PresentService key={s.id} {...s} />)}
        </div>
        <Catalogues/>
    </div>;
}

export default Services;