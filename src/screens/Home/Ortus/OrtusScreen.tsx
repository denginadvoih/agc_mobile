import React from 'react';
import classes from './ortusscreen.module.scss';
import {OrtusScreens} from "../../../demoData/ortus/ortusScreens";

const OrtusScreen = ({data}: { data: OrtusScreens }) => {
    return (
        <div className={classes.screen}>
            <div className={classes.wrapper}>
                <div className={classes.title}>{data.title}</div>
                <div className={classes.description}>
                    {data.description}
                </div>
                <div className={classes.item}>{data.extraItem}</div>
                <div className={classes.image}>
                    <img src={data.image} alt=""/>
                </div>
            </div>

        </div>
    );
};

export default OrtusScreen;