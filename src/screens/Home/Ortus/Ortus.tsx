import React, {useState} from "react";
import {isAndroid, isIOS} from "react-device-detect";
import classes from './ortus.module.scss';
import {ortusScreens} from "../../../demoData/ortus/ortusScreens";
import OrtusScreen from "./OrtusScreen";
import {Pagination} from "swiper/modules";
import {Swiper, SwiperSlide} from "swiper/react";
import 'swiper/css';
import 'swiper/scss/pagination';

const Ortus = () => {

    const androidLink = "https://play.google.com/store/apps/details?id=ru.ortus.assistant";
    const iosLink = "https://itunes.apple.com/ru/app/id1189488678";
    const [currentPresentId, setCurrentPresentId] = useState(0);
    console.log(ortusScreens);
    return (
        <div className={classes.ortus}>
            <Swiper
                spaceBetween={0}
                slidesPerView={1}
                onSlideChange={(swiper) => setCurrentPresentId(swiper.realIndex)}
                onSwiper={(swiper) => console.log(swiper)}
                loop
                modules={[Pagination]}
                pagination={{
                    clickable: true,
                    el: "#ortusPaginatorElement",
                    horizontalClass: classes.pagination,
                    bulletClass: classes.bullet,
                    bulletActiveClass: classes.active
                }}
            >
                <SwiperSlide>
                    <div className={classes.ortusMainScreen}>
                        <div className={classes.newScreen}>
                            <div className={classes.title}>Электронная лучше</div>
                            <div className={classes.title2}>Сервисная книжка<br/>вашего автомобиля</div>
                            {isAndroid && <a href={androidLink} className={classes.downloadLink}>Скачать Ortus</a>}
                            {isIOS && <a href={iosLink} className={classes.downloadLink}>Скачать Ortus</a>}
                        </div>
                    </div>
                </SwiperSlide>
                {ortusScreens.map((ortusScreen, index) => <SwiperSlide key={index}><OrtusScreen
                    data={ortusScreen}></OrtusScreen></SwiperSlide>)}
                <div className={classes.buttonPosition} id="childId" data-slidenumber={currentPresentId}>
                    <div id={"ortusPaginatorElement"}/>
                    <div id="readonButton" className={classes.readOn}>
                    </div>
                </div>
            </Swiper>

        </div>

    );
}
export default Ortus;