import React from "react";
import PropTypes from 'prop-types';
import 'swiper/css';
import classes from './giftSections.module.scss'
import GiftSection from "./GiftSection";

const GiftSections = ({ giftSections }) => {


    return <div className={classes.screen}>
        <div className={classes.header}>
            Акции, подарки<br/>и розыгрыши
        </div>
        <div className={classes.giftSections}>
            {giftSections.map(g => <GiftSection {...g} />)}
        </div>
    </div>;
}

GiftSections.propTypes = {
    giftSections: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        image: PropTypes.string,
    })),

}

export default GiftSections;