import React from "react";
import PropTypes from 'prop-types';
import 'swiper/css';
import classes from './giftSection.module.scss'
import Button from "components/Button";
import {ButtonStyles} from "components/Button/Button";
import {ROUTES} from "../../../../routes";
import {useNavigate} from "react-router-dom";

const GiftSection = ({title, image}) => {
    const navigate = useNavigate();
    return (
        <div className={classes.card}>
            <div className={classes.image}>
                <img src={image} alt={''}/>
            </div>
            <div className={classes.title}>{title}</div>
            <Button styles={[ButtonStyles.blue, ButtonStyles.small]}
                    handler={() => navigate(ROUTES.readOn)}>Подробно</Button>
        </div>
    );
}

GiftSection.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    image: PropTypes.string
}

export default GiftSection;