import React, {useEffect, useState} from "react";
import {YMaps} from '@pbe/react-yandex-maps';
import classes from './contacts.module.scss'
import Button from "components/Button";
import cx from "classnames";
import {ButtonStyles} from "components/Button/Button";
import {Link} from "react-router-dom";
import {ROUTES} from "../../../routes";
import {filials} from "../../../demoData/filials/filials";
import Filial from "./Filial";
import SendPhone from "./SendPhone";
import {YA_API_KEY} from "../../../utils/constants";

const Contacts = () => {
    const [mapTab, setMapTab] = useState(0);
    const [openFooter, setOpenFooter] = useState(false);
    const toggleFooter = () => {
        setOpenFooter(_ => !_);
    }
    useEffect(() => {
        if (openFooter) {
            const element = document.getElementById('copyRights');
            if (element) {
                console.log('скроллим до элемента');
                element.scrollIntoView({behavior: 'smooth'});
            }
        }
    }, [openFooter])

    return <YMaps query={{ apikey: YA_API_KEY }}>
        <div className={classes.screen}>
            <div className={classes.header}>Контакты</div>
            <div className={classes.contacts}>
                <div className={classes.greeting}>Мы рады вам <br/>7 дней в неделю</div>
                <div className={classes.time}>09:00-20:00</div>
                <div className={classes.phone}>
                    <a href="tel:+74232222999">+7 (423) 2222-999</a>
                </div>
                <div className={classes.phone + ' ' + classes.whatsapp}>
                    <a href="https://wa.me/79140773596">+7 (423) 2222-999</a>
                </div>
            </div>
            <div className={classes.filials}>
                <div className={classes.header}>Наши филиалы</div>
                <div className={classes.tabs}>
                    <div className={cx({[classes.tab]: true, [classes.active]: mapTab === 0})}
                         onClick={() => setMapTab(0)}>ул. Шилкинская, 32 а
                    </div>
                    <div className={cx({[classes.tab]: true, [classes.active]: mapTab === 1})}
                         onClick={() => setMapTab(1)}>ул. Снеговая, 1 стр 3
                    </div>
                </div>
                <div className={classes.maps}>
                    <Filial {...filials.shilkinskaya} active={mapTab === 0}/>
                    <Filial {...filials.snegovaia} active={mapTab === 1} />
                </div>
            </div>
            <SendPhone />
            <div className={classes.footer}>
                <div className={classes.text1}>Лучше один раз увидеть,<br/>чем сто раз услышать</div>
                <div className={classes.divider}/>
                <div className={classes.text2}>Приезжайте и почувствуйте разницу</div>
                <div className={classes.hearts}/>
                <div className={classes.buttons}>
                    <Button handler={() => {
                        window.location.href = 'https://ortus.ru/w/2222999';
                    }} styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}>Записаться</Button>
                    <Button handler={() => null} styles={[ButtonStyles.blueBorder, ButtonStyles.fullWidth]}>Получить профессиональную консультацию</Button>
                </div>
                <div className={classes.seeYou}>До встречи!</div>
            </div>
            <div className={cx({[classes.copyRights]: true, [classes.open]: openFooter})} id={'copyRights'}>
                <div className={classes.title}>
                    © 2023 Специализированный автосервис<br/>
                    АвтоГарантСити. Все права защищены
                </div>
                <div className={classes.arrow} onClick={toggleFooter}></div>
                <div className={classes.expanded}>
                    <Link to={ROUTES.readOn} className={classes.link + ' ' + classes.blue}>Политика конфиденциальности</Link>
                    <div className={classes.description}>
                        Вся представленная на сайте информация стоимости
                        сервисного обслуживания носит исключительно
                        информационный характер и не является публичной офертой,
                        определяемой положениями ст. 437 (2) ГК Рф.
                    </div>
                </div>
            </div>
        </div>
    </YMaps>;
}
export default Contacts;