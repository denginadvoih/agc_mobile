import React, {useEffect, useState} from "react";
import classes from './filial.module.scss';
import cx from "classnames";
import YouTubeWrapper from "components/YouTubeWrapper";
import Button from "components/Button";
import {ButtonStyles} from "components/Button/Button";
import Close from "components/Close";
import {Map, Button as YaButton} from "@pbe/react-yandex-maps";
import {Pagination} from "swiper/modules";
import {Swiper, SwiperSlide} from "swiper/react";

const FILIAL_TABS = {map: 'map', parking: 'parking', routeVideos: 'routeVideos', mainPhoto: 'mainPhoto'};
const FILIAL_NAMES = {map: 'Карта', parking: 'Парковка', routeVideos: 'Как проехать', mainPhoto: 'Фото'};

const Filial = ({photo, map, yaMap, parking, routeVideos, location, active}) => {
    const [tab, setTab] = useState(FILIAL_TABS.mainPhoto);
    const [mapState, setMapState] = useState();
    const [showRoutes, setShowRoutes] = useState(false);

    useEffect(() => {
        const scrollAction = () => {
            setShowRoutes(false);
        };
        document.addEventListener('scroll', scrollAction, true);
        return () => {
            document.removeEventListener('scroll', scrollAction);
        }
    }, []);

    const tabDiv = (tabLabel) =>
        <div className={cx({[classes.tab]: true, [classes.active]: tab === tabLabel})}
             onClick={() => setTab(tabLabel)} style={{flex: FILIAL_NAMES[tab].length}}>{FILIAL_NAMES[tabLabel]}</div>

    return <div className={classes.filial + ' ' + (active ? classes.active : '')}>
        <div className={classes.tabs}>
            {tabDiv(FILIAL_TABS.mainPhoto)}
            {tabDiv(FILIAL_TABS.map)}
            {tabDiv(FILIAL_TABS.routeVideos)}
            {tabDiv(FILIAL_TABS.parking)}
        </div>
        <div className={classes.body}>
            {tab === FILIAL_TABS.map ?
                <div className={classes[FILIAL_TABS.map]}>
                    {yaMap ?
                        <Map {...yaMap} state={mapState}>
                            <YaButton
                                options={{maxWidth: 128}}
                                data={{content: "Вернуться..."}}
                                defaultState={{selected: false}}
                                onClick={() => {
                                    setMapState(yaMap.defaultState);
                                }}
                            />
                        </Map> : (map ? <img src={map} alt='Карта нахождения филиала'/> : null)
                    }
                    <div className={classes.buttonPosition}>
                        <Button styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}
                                handler={() => setShowRoutes((_) => !_)}>
                            <div className={classes.routeIcon}/>
                            Проложить маршрут
                        </Button>
                    </div>
                    <div className={cx({[classes.floatWindow]: true, [classes.open]: showRoutes})}>
                        <Close className={classes.close} handler={() => setShowRoutes(false)}/>
                        <h2>Воспользоваться сервисом</h2>
                        <a href={`yandexmaps://build_route_on_map?lat_to=${location[0]}&lon_to=${location[1]}`}
                           className={classes.button}>
                            <Button
                                styles={[ButtonStyles.rounded, ButtonStyles.white, ButtonStyles.shadow, ButtonStyles.noBorder]}
                                handler={() => null}>
                                <div className={classes.yandexMap}/>
                            </Button>
                            <div className={classes.text}>Yandex<br/>карты</div>
                        </a>
                        <a href={`google.navigation:q=${location[0]}, ${location[1]}&amp;mode=d`}
                           className={classes.button}>
                            <Button
                                styles={[ButtonStyles.rounded, ButtonStyles.white, ButtonStyles.shadow, ButtonStyles.noBorder]}
                                handler={() => null}>
                                <div className={classes.google}/>
                            </Button>
                            <div className={classes.text}>Google Maps</div>
                        </a>
                        <a href={`https://2gis.ru/vladivostok/routeSearch/rsType//from/╎╎/to/${location[0]},${location[1]}╎╎`}
                           className={classes.button}>
                            <Button
                                styles={[ButtonStyles.rounded, ButtonStyles.white, ButtonStyles.shadow, ButtonStyles.noBorder]}
                                handler={() => null}>
                                <div className={classes.twogis}/>
                            </Button>
                            <div className={classes.text}>2ГИС</div>
                        </a>

                    </div>
                </div>
                : null}
            {tab === FILIAL_TABS.mainPhoto ?
                <div className={classes[FILIAL_TABS.mainPhoto]}>
                    <Swiper
                        spaceBetween={0}
                        slidesPerView={1}
                        loop
                        autoHeight={true}
                        modules={[Pagination]}
                        className={classes.swiper}
                        pagination={{
                            clickable: true,
                            horizontalClass: classes.pagination,
                            bulletClass: classes.bullet,
                            bulletActiveClass: classes.active
                        }}
                    >
                        {
                            (photo || []).map((ph, key) => <SwiperSlide
                                key={key}><img src={ph} alt={''}/></SwiperSlide>)
                        }
                    </Swiper>
                </div>
                : null}
            {tab === FILIAL_TABS.routeVideos ?
                routeVideos.map(v => <div className={classes.video}>{<YouTubeWrapper {...v}/>}</div>)
                : null}
            {tab === FILIAL_TABS.parking ?
                <div className={classes[FILIAL_TABS.parking]}>
                    <Swiper
                        spaceBetween={0}
                        slidesPerView={1}
                        loop
                        autoHeight={true}
                        modules={[Pagination]}
                        className={classes.swiper}
                        pagination={{
                            clickable: true,
                            horizontalClass: classes.pagination,
                            bulletClass: classes.bullet,
                            bulletActiveClass: classes.active
                        }}
                    >
                        {
                            (parking || []).map((ph, key) => <SwiperSlide
                                key={key}><img src={ph} alt={'Фото парковки'}/></SwiperSlide>)
                        }
                    </Swiper>
                </div>
                : null}
        </div>

    </div>
}
export default Filial;