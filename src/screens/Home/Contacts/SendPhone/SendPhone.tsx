import React, { useRef, useState } from "react";
import ReCAPTCHA from 'react-google-recaptcha';
import classes from './sendPhone.module.scss';
import InputMask from 'react-input-mask';
import Button from "components/Button";
import { ButtonStyles } from "components/Button/Button";
import { FETCH_RESULT, RECAPTCHA_KEY } from "utils/constants";

interface SendPhoneProps {
    mailIsSent: string;
    sendMail: (data: { phone: string, name: string, token: string }) => void;
    sendMailReset: () => void;
}

const SendPhone: React.FC<SendPhoneProps> = ({ mailIsSent, sendMail, sendMailReset }) => {
    const [phone, setPhone] = useState<string>('');
    const [name, setName] = useState<string>('');
    const reCaptchaRef = useRef<ReCAPTCHA>(null);

    const sendData = async () => {
        if (reCaptchaRef.current) {
            const token = await reCaptchaRef.current.executeAsync();
            const data = {
                phone: phone,
                name: name,
                token: token!
            };
            if (token !== null) {
                sendMail(data);
            }
        }
    }

    const validData = (): boolean => {
        return name !== '' && phone.replace(/[^+\d]/g, '').length === 12;
    }

    return (
        <div className={classes.sendPhone}>
            <div className={classes.header}>Получите профессиональную консультацию</div>
            <div className={classes.text}>Оставьте свои контактные данные и в ближайшее время мы с вами свяжемся</div>
            <input placeholder={'Ваше имя'} type='text' value={name} onChange={(e) => setName(e.target.value)} />
            <InputMask
                placeholder={'+7 000 000 00 00'}
                value={phone}
                mask="+7 999 999 99 99"
                onChange={(e) => setPhone(e.target.value)}
            />
            <Button
                handler={() => sendData()}
                disabled={!validData()}
                styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}
            >
                Получить консультацию
            </Button>
            <div className={classes.privateAgree}>Отправляя заявку я даю согласие на обработку <span>Персональных данных</span></div>

            <ReCAPTCHA
                sitekey={RECAPTCHA_KEY}
                size="invisible"
                ref={reCaptchaRef}
            />
            {mailIsSent === FETCH_RESULT.success ? (
                <div className={classes.shadowWrapper} onClick={(e) => {
                    e.stopPropagation();
                    sendMailReset();
                }}>
                    <div className={classes.modal} onClick={(e) => {
                        e.stopPropagation();
                        sendMailReset();
                    }}>
                        <div className={classes.button} />
                        <div className={classes.header}>Спасибо за обращение!</div>
                        <div className={classes.text}>В ближайшее время мы с вами свяжемся</div>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default SendPhone;
