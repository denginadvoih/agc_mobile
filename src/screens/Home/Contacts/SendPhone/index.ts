import {connect} from "react-redux";
import SendPhone from './SendPhone';
import { sendMail, sendMailReset } from "../../../../redux/slices/dataSlice";

const mapStateToProps = (state: {data: {mailIsSent: string; fetching: boolean}}) => {
    return {
        mailIsSent: state.data.mailIsSent,
        fetching: state.data.fetching
    }
};

export default connect(mapStateToProps, { sendMail, sendMailReset })(SendPhone);