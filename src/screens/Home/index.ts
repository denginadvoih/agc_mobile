import {connect} from "react-redux";
import { changeStartStatus } from "redux/slices/hangarSlice";
import Home from './Home';

const mapStateToProps = (state: { hangar: { startStatus: boolean; }; }) => {
    return {
        startStatus: state.hangar.startStatus
    }
};

export default connect(mapStateToProps, { changeStartStatus })(Home);