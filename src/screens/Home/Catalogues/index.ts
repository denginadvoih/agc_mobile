import {connect} from "react-redux";
import { fetchCatalogues, fetchServices } from "../../../redux/slices/dataSlice";
import Catalogues from './Catalogues';
import {Services} from "./Catalogues";

const mapStateToProps = (state: {data: { catalogues: any; services: Services }}) => {
    return {
        rawCatalogues: state.data.catalogues,
        rawServices: state.data.services
    }
};

export default connect(mapStateToProps, { fetchCatalogues, fetchServices })(Catalogues);