import React from "react";
import 'swiper/css';
import classes from './carCenter.module.scss'
//import Button from "components/Button";
//import {ButtonStyles} from "components/Button/Button";
//import {ROUTES} from "../../../../routes";
//import {useNavigate} from "react-router-dom";
import {CarCenterDto} from "../CarCenters";

const CarCenter = (props: CarCenterDto) => {
    const {title, label, image} = props;
    //const navigate = useNavigate();
    return (
        <div className={classes.card + ' ' + classes[label]}>
            <div className={classes.image}>
                <img src={image} alt={''}/>
            </div>
            <div className={classes.title}>Специализированный центр</div>
            <div className={classes.title2}>{title}</div>
            <div className={classes.title3}>Профессиональный ремонт</div>
            {/*<Button styles={[ButtonStyles.blue, ButtonStyles.small]}  handler={() => navigate(ROUTES.readOn)}>Подробно</Button>*/}
        </div>
    );
}

export default CarCenter;