import React from "react";
import classes from './carCenters.module.scss'
//import CarCenter from "./CarCenter";
import CarSplitter from "./CarSplitter";

export interface CarCenterDto {
    label: string;
    title: string;
    image: string;
}

const CarCenters = ({carCenters}: {
    carCenters:
        CarCenterDto[]
}) => {
    const toyota = carCenters.find(c => c.label === 'toyota');
    const lexus = carCenters.find(c => c.label === 'lexus');
    return <div className={classes.screen}>
        <div className={classes.divisions}>
            {toyota && lexus && <CarSplitter
                car1={toyota}
                car2={lexus}
            />}

            {/*{carCenters.filter(c => ['toyota', 'lexus'].indexOf(c.label) === -1)
                .map(c => <CarCenter key={c.id} {...c} />)}*/}
        </div>
    </div>;
}

export default CarCenters;