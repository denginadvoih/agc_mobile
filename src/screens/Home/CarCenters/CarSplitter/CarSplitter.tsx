import React, {useState} from "react";
import 'swiper/css';
import classes from './carSplitter.module.scss'
import {CarCenterDto} from "../CarCenters";
//import Button from "components/Button";
//import {ButtonStyles} from "components/Button/Button";
//import {ROUTES} from "../../../../routes";
//import {useNavigate} from "react-router-dom";

const CarSplitter = ({car1, car2}: {car1: CarCenterDto; car2: CarCenterDto;}) => {
    const [showFirstCar, setShowFirstCar] = useState(true);
    //const navigate = useNavigate();
    return (
        <div className={classes.card}>
            <div className={classes.image + ' ' + (showFirstCar ? classes.selected : '')}>
                <img src={car1.image} alt={''}/>
            </div>
            <div className={classes.image + ' ' + (!showFirstCar ? classes.selected : '')}>
                <img src={car2.image} alt={''}/>
            </div>
            <div className={classes.title}>Специализированный центр</div>
            <div className={classes.title2 + ' ' + (showFirstCar ? classes.first : classes.second)}>
                <div onClick={() => setShowFirstCar(true)}>{car1.title}</div>
                <div onClick={() => setShowFirstCar(false)}>{car2.title}</div>
            </div>
            <div className={classes.title3}>Профессиональный ремонт</div>
            {/*<Button styles={[ButtonStyles.blue, ButtonStyles.small]} handler={() => navigate(ROUTES.readOn)}>Подробно</Button>*/}
            <div className={classes.soon}>СКОРО</div>
        </div>
    );
}

export default CarSplitter;