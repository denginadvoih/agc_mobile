import React, {useLayoutEffect, useState} from "react";
import PropTypes from 'prop-types';
import {Swiper, SwiperSlide} from 'swiper/react';
import {Pagination} from 'swiper/modules';
// Import Swiper styles
import 'swiper/css';
import 'swiper/scss/pagination';
import classes from './acquaint.module.scss'
import Present from "../../../components/Present";
import Reviews from "./Reviews";
import {divisions} from "demoData/divisions/divisions";
import {presentCarCenters} from 'demoData/presentCarCenters/presentCarCenters';
import Services from "../Services";
import Divisions from "../Divisions";
import CarCenters from "../CarCenters";
import Ortus from "../Ortus";
import {SCREENS} from "../../../utils/constants";
import {PresentScreen} from "../../../types/global";

const Acquaint = ({presents}: { presents: PresentScreen[] }) => {
    const [offsetPosition, setOffsetPosition] = useState(0);
    const [currentPresentId, setCurrentPresentId] = useState(0);

    const getButtonsPosition = () => {
        try {
            const parentPos = document.getElementById('parentId')?.getBoundingClientRect();
            const childPos = document.getElementById('childId')?.getBoundingClientRect();
            const buttonPos = document.getElementById('readonButton')?.getBoundingClientRect();
            const footerPos = document.getElementById('footer')?.getBoundingClientRect();
            return (childPos !== undefined && parentPos !== undefined && buttonPos !== undefined && footerPos !== undefined)
                ? childPos.top + (childPos.height - ((childPos.height - buttonPos.height) / 2)) + footerPos.height - parentPos.top
                : 0;
        } catch (e) {
            return 0;
        }
    }

    useLayoutEffect(() => {
        const windowInnerHeight = window.innerHeight;
        const buttonPos = getButtonsPosition();
        const offset = windowInnerHeight - buttonPos;
        setOffsetPosition(offset);
    }, []);

    return <>
        <div className={classes.screen} id="parentId">
            <div id={SCREENS.acquaint} style={{position: 'absolute', top: -offsetPosition + 'px'}}/>
            <div className={classes.present}>
                <Swiper
                    spaceBetween={0}
                    slidesPerView={1}
                    onSlideChange={(swiper) => setCurrentPresentId(presents[swiper.realIndex].id)}
                    onSwiper={(swiper) => console.log(swiper)}
                    loop
                    modules={[Pagination]}
                    pagination={{
                        clickable: true,
                        el: "#paginatorElement",
                        horizontalClass: classes.pagination,
                        bulletClass: classes.bullet,
                        bulletActiveClass: classes.active
                    }}
                >
                    {
                        presents.map((present, key) => <SwiperSlide key={key}><Present {...present} /></SwiperSlide>)
                    }
                    <div className={classes.buttonPosition} id="childId" data-slidenumber={currentPresentId}>
                        <div id={"paginatorElement"}/>
                        <div id="readonButton" className={classes.readOn}/>
                        <div className={classes.spacer}/>
                    </div>
                </Swiper>

            </div>
        </div>
        <Services/>
        <Divisions divisions={divisions.filter(d => d.hidden !== true)}/>
        <CarCenters carCenters={presentCarCenters}/>
        <Ortus/>
        <Reviews/>
    </>;
}

Acquaint.propTypes = {
    presents: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        description: PropTypes.string
    }))
}


export default Acquaint;