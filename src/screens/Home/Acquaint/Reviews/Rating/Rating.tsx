import React from "react";
import classes from './rating.module.scss'

const Rating = ({origin, value, url}: {origin: string; value: number; url: string;}) => {
    const navigate = (url: string) => {
        window.location.href = url;
    }

    return <div className={classes.rating + ' ' + classes[origin]} onClick={() => url && navigate(url)}>
        <div className={classes.stars}>
            <div className={classes.value} style={{width: (value / (5 / 100)) + '%'}}></div>
        </div>
        <div className={classes.value}>{value}</div>
    </div>;
}

export default Rating;