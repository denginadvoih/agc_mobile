import {connect} from "react-redux";
import {fetchReviews} from "redux/slices/dataSlice";
import Reviews from './Reviews';
import {ReviewDto} from "types/global";

const mapStateToProps = (state: { data: { reviews: ReviewDto[] } }) => {
    return {
        reviews: state.data.reviews
    }
};

export default connect(mapStateToProps, {fetchReviews})(Reviews);