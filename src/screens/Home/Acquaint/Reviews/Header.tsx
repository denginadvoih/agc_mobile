import React from "react";
import classes from './header.module.scss'

const Header = () => {
    return <div className={classes.header}>Честные отзывы наших гостей</div>;
}

export default Header;