import React, {useEffect, useState} from "react";
import {ReviewDto} from 'types/global';
import {Swiper, SwiperSlide, useSwiper} from 'swiper/react';
import {Pagination} from 'swiper/modules';
// Import Swiper styles
import 'swiper/css';
import classes from './reviews.module.scss'
import Header from "./Header";
import Review from "./Review";
import {ButtonStyles} from "components/Button/Button";
import Button from "components/Button";
import Rating from "./Rating/Rating";
import './pagination.scss';

const Reviews = ({reviews, fetchReviews}: { reviews: ReviewDto[]; fetchReviews: () => void }) => {
    const [startScreen, setStartScreen] = useState(true);
    useEffect(() => {
        fetchReviews();
    }, [fetchReviews]);

    const scrollOnSwipe = () => {
        const element = document.getElementById('reviewsPosition');
        if (element) {
            element.scrollIntoView({behavior: 'smooth'});
        }
    };
    const scrollOnEndSwipe = () => {
        const element = document.getElementById('reviewsPositionOnStart');
        if (element) {
            element.scrollIntoView({behavior: 'smooth'});
        }
    };

    const ratings = [
        {label: 'ortus', value: 4.9},
        {label: 'vl', value: 4.8, url: 'https://www.vl.ru/avtogarant#comments'},
        {label: 'yandex', value: 4.9, url: 'https://yandex.ru/profile/1267603716'},
        {
            label: 'google',
            value: 4.6,
            url: 'https://www.google.com/maps/place/Avtogarantsiti/@43.1364018,131.9159173,17z/data=!4m10!1m2!2m1!1z0LDQstGC0L7Qs9Cw0YDQsNC90YLRgdC40YLQuA!3m6!1s0x5fb39268ab0a0551:0x4bdf65aee29674f1!8m2!3d43.1364018!4d131.9206809!15sChzQsNCy0YLQvtCz0LDRgNCw0L3RgtGB0LjRgtC4kgEKY2FyX3JlcGFpcuABAA!16s%2Fg%2F1hf70xsn4?entry=ttu'
        },
        {
            label: 'twogis',
            value: 4.5,
            url: 'https://2gis.ru/vladivostok/branches/3518974079836943/firm/3518965490005693'
        }
    ];

    return <div className={classes.screen}>
        <div className={classes.reviews}>
            <div id="reviewsPositionOnStart" className={classes.reviewsPosition}/>
            <Header/>
            <div id="reviewsPosition" className={classes.reviewsPosition}/>
            <div className={classes.swipeWrapper + ' ' + (startScreen ? classes.startScreen : '')}>
                <Swiper
                    spaceBetween={0}
                    slidesPerView={1}
                    onSlideChangeTransitionEnd={(swiper) => {
                        setStartScreen(swiper.realIndex === 0);
                        if (swiper.previousIndex !== 0 && swiper.realIndex === 0) {
                            scrollOnEndSwipe();
                        } else {
                            if (swiper.previousIndex !== 0) {
                                scrollOnSwipe();
                            }
                        }
                    }}

                    loop
                    autoHeight={true}
                    modules={[Pagination]}
                    pagination={{clickable: true}}
                >
                    <SwiperSlide>
                        <div className={classes.ratings}>
                            <div className={classes.subHeader}>
                                <div>
                                    Рейтинги
                                </div>
                                <ButtonForFirstSlide/>
                            </div>
                            <div className={classes.ratingsList}>
                                {ratings.map((rating) => {
                                    return <Rating key={rating.label} origin={rating.label} value={rating.value}
                                                   url={rating.url || ''}/>;
                                })}
                            </div>
                        </div>
                    </SwiperSlide>
                    {
                        (reviews || []).map((review, key) => <SwiperSlide
                            key={key}><Review {...review} /></SwiperSlide>)
                    }
                </Swiper>
            </div>
        </div>
    </div>;
}

const ButtonForFirstSlide = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.arrowRight, ButtonStyles.circle, ButtonStyles.gray]} handler={() => {
        swiper.slideNext();
    }}/>
}

export default Reviews;