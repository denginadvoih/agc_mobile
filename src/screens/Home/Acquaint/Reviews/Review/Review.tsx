import React from "react";
import classes from './review.module.scss'
import {useSwiper} from "swiper/react";
import Button from "components/Button";
import {ButtonStyles} from "components/Button/Button";

const Review = ({origin, image}: {origin: string; image: string;}) => {
    const swiper = useSwiper();
    return <div className={classes.screen}>
        <div className={classes.reviewScreen}>
            <div className={classes.subHeader}>
                <div>
                    <div>Что о нас говорят</div>
                    <div>Источник: {origin}</div>
                </div>
                <Button styles={[ButtonStyles.arrowRight, ButtonStyles.circle, ButtonStyles.gray]} handler={() => swiper.slideNext()}></Button>
            </div>
            <div className={classes.image}>
                <img src={image} alt=''/>
            </div>
        </div>
    </div>;
}

export default Review;