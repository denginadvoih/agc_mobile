import React from "react";
import 'swiper/css';
import classes from './divisions.module.scss'
import Division from "./Division";
import {DivisionType} from "./Division/Division";

const Divisions = ({divisions}: {divisions: DivisionType[]}) => {
    return <>
        <div className={classes.screen}>
            <div className={classes.header}>
                <div className={classes.logo}/>
                <div className={classes.text}>Сеть специализированных<br/>автосервисов</div>
            </div>
            <div className={classes.divisions}>
                {divisions.map(g => <Division key={g.id} {...g} />)}
            </div>
        </div>
    </>;
}

export default Divisions;