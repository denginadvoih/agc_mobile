import React from "react";
import 'swiper/css';
import classes from './division.module.scss'
import Button from "components/Button";
import {ButtonStyles} from "components/Button/Button";

export interface DivisionType {
    id: number;
    title: string;
    description: string;
    image: string;
    url?: string;
    hidden?: boolean;
}
const Division = ({id, title, description, image, url} : DivisionType) => {
    const outerNavigate = (url: string) => {
        window.location.href = url;
    }
    return (
        <div className={classes.card} onClick={() => url && outerNavigate(url)}>
            <div className={classes.image}>
                <img src={image} alt={''}/>
            </div>
            <div className={classes.title}>{title}</div>
            <div className={classes.title2}>{description}</div>
            {id === 1 && <Button styles={[ButtonStyles.small]} handler={() => url && outerNavigate(url)}>Познакомиться<br/>поближе</Button>}
            {/*<Button styles={[ButtonStyles.blue, ButtonStyles.small]} handler={() => navigate(ROUTES.readOn)}>Подробно</Button>*/}
        </div>
    );
}

export default Division;