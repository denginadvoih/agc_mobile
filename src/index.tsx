import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import Home from './screens/Home';
import reportWebVitals from './reportWebVitals';
import './styles/styles.css';
import Plug from './screens/Plug';
import store from './redux/store';
import { ROUTES } from './routes';
import Gift from './screens/Home/Services/PresentService/Gift';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
    <Provider store={store}>
        <BrowserRouter>
            <Routes>
                <Route path={ROUTES.main} element={<Home />} />
                <Route path={ROUTES.readOn} element={<Plug />} />
                <Route path={ROUTES.serviceGift(':serviceCardId')} element={<Gift />} />
            </Routes>
        </BrowserRouter>
    </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
