// noinspection SpellCheckingInspection
export const ROUTES = {
    main: '/',
    readOn: '/readon',
    serviceGift: (serviceId) => '/services/' + serviceId + '/gift'
}