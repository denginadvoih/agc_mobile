import { setAjaxError } from '../redux/slices/hangarSlice';

export const API_URL = 'https://www.autogarantcity.ru';

interface ApiOptions {
    method?: string;
    mode?: RequestMode;
    headers?: HeadersInit;
    body?: BodyInit | null;
}

interface Dispatch {
    (arg: any): void;
}

export const api = async (
    uri: string,
    data: ApiOptions,
    dispatch?: Dispatch,
    fileUpload = false
): Promise<any> => {
    let opts: ApiOptions = {
        method: 'get',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
        },
    };

    if (!fileUpload) {
        (opts.headers as Record<string, string>)['Content-Type'] = 'application/json';
    }

    const token = localStorage.getItem('token');
    if (token) {
        (opts.headers as Record<string, string>)['X-Api-Token'] = token;
    }

    opts = { ...opts, ...data };
    opts.headers = new Headers(opts.headers);

    try {
        const response = await fetch((uri.indexOf('http') === 0 ? '' : API_URL) + uri, opts);
        if (response.status !== 200) {
            if (dispatch) {
                const res = await response.json();
                dispatch(setAjaxError({ response: res, status: response.status, url: response.url }));
                return false;
            }
        } else {
            return await response.json();
        }
    } catch (error) {
        console.error("API call failed:", error);
        if (dispatch) {
            dispatch(setAjaxError({ response: error, status: 500, url: uri }));
        }
        return false;
    }
};

export const http_build_query = (
    formdata: Record<string, any>,
    numeric_prefix?: string,
    arg_separator: string = '&'
): string => {
    let key: string, use_val: string, use_key: string;
    let i = 0;
    const tmp_arr: string[] = [];

    for (key in formdata) {
        if (formdata[key] !== undefined && formdata[key] !== null && formdata[key] !== '') {
            use_key = encodeURIComponent(key);
            use_val = encodeURIComponent(formdata[key].toString());
            if (numeric_prefix && !isNaN(parseInt(key))) {
                use_key = numeric_prefix + i;
            }
            tmp_arr[i] = use_key + '=' + use_val;
            i++;
        }
    }
    return tmp_arr.join(arg_separator);
};
