import React from "react";
import classes from "./close.module.scss";

const Close = ({className, handler}: {className: string; handler: () => void})=> {
    return (
        <div className={classes.close + (className ? ' ' + className : '')} onClick={() => {
           handler();
        }}/>
    );
}

export default Close;