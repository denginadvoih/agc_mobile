import React, { ReactNode } from "react";
import classes from "./screen.module.scss";
import cx from 'classnames';

interface ScreenProps {
    children: ReactNode;
    active?: boolean;
}

const Screen = ({ children, active = false }: ScreenProps) => {
    return (
        <div className={cx({ [classes.screen]: true, [classes.active]: active })}>
            {children}
        </div>
    );
}

export default Screen;
