import React from "react";
import classes from "./footer.module.scss";
import cx from 'classnames';
import {Swiper, SwiperSlide} from "swiper/react";
import 'swiper/css';

const Footer = ({active = false}) => {
    return (
        <div className={cx({[classes.footer]: true, [classes.active]: active})} id="footer">
            <div className={classes.wrapper}>
            <Swiper
                spaceBetween={0}
                slidesPerView={7}
                onSlideChange={() => {}}
                loop
                autoHeight={true}
            >
                <SwiperSlide>
                    <div className={classes.icon} data-mark={'toyota'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={classes.icon} data-mark={'lexus'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={classes.icon} data-mark={'nissan'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={classes.icon} data-mark={'honda'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>

                <SwiperSlide>
                    <div className={classes.icon} data-mark={'subaru'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={classes.icon} data-mark={'mitsubishi'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={classes.icon} data-mark={'hyundai'}
                         onClick={() => window.location.href = 'https://ortus.ru/w/2222999'}/>
                </SwiperSlide>

            </Swiper>
            </div>
        </div>
    );
}

export default Footer;