import React from "react";
import { isAndroid, isIOS } from "react-device-detect";
import { Container, Row, Col, Button } from "react-bootstrap";

const DownloadLink: React.FC = () => {
    const androidLink = "https://play.google.com/store/apps/details?id=com.example.android";
    const iosLink = "https://apps.apple.com/us/app/example-ios/id123456789";

    return (
        <Container>
            <Row className="justify-content-center mt-5">
                <Col md={6} className="text-center">
                    {isAndroid && (
                        <Button variant="primary" href={androidLink} target="_blank">
                            Download for Android
                        </Button>
                    )}
                    {isIOS && (
                        <Button variant="primary" href={iosLink} target="_blank">
                            Download for iPhone
                        </Button>
                    )}
                    {!isAndroid && !isIOS && (
                        <p>Unsupported device. Please visit the download page from an Android or iPhone device.</p>
                    )}
                </Col>
            </Row>
        </Container>
    );
};

export default DownloadLink;
