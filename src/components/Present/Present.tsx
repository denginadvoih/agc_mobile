import React from "react";
import cn from 'classnames';
import classes from "./present.module.scss";
import {PresentScreen} from "../../types/global";

const Present = ({id, title, description, image, theme, component}: PresentScreen) => {
    return (
        <div className={cn({[classes.present]: true, [classes[String(theme)]]: true})} data-id={id}>
            <div className={classes.image}>
                <img src={image} alt=''/>
            </div>
            <div className={classes.textPosition1}>
                <div className={classes.title}>{title}</div>
            </div>
            <div className={classes.textPosition2}>
                <div className={classes.description}>{description}</div>
            </div>
            {component}
        </div>
    );
}

export default Present;