import React, { useState } from "react";
import classes from "./youtubewrapper.module.scss";
import YouTube, { YouTubeProps } from "react-youtube";

interface YouTubeWrapperProps {
    id: string;
    title: string;
    preview: string;
}

const YouTubeWrapper: React.FC<YouTubeWrapperProps> = ({ id, title, preview }) => {
    const [player, setPlayer] = useState<any>();
    const [onPlay, setOnPlay] = useState(false);

    const onPlayerReady: YouTubeProps['onReady'] = (event) => {
        const player = event.target;
        setPlayer(player);
    };

    const options = {
        height: "100%",
        width: "100%",
        playerVars: {
            autoplay: 0,
        },
    };

    return (
        <div className={`${classes.video} ${onPlay ? classes.onPlay : ''}`}>
            <YouTube
                videoId={id}
                opts={options}
                onReady={onPlayerReady}
                className={classes.wrapper}
            />
            <div className={classes.preview}>
                <img src={preview} alt={title} />
            </div>
            <div className={classes.title}>{title}</div>
            <div className={classes.play} onClick={() => {
                setOnPlay(true);
                player?.playVideo();
            }} />
        </div>
    );
}

export default YouTubeWrapper;
