import React, { ReactNode } from "react";
import classes from "./button.module.scss";
import cx from 'classnames';

export enum ButtonStyles {
    small = 'small',
    blue = 'blue',
    gray = 'gray',
    lightGray = 'lightGray',
    circle = 'circle',
    arrowLeft = 'arrowLeft',
    arrowRight = 'arrowRight',
    fullWidth = 'fullWidth',
    blueBorder = 'blueBorder',
    whiteBorder = 'whiteBorder',
    rounded = 'rounded',
    white = 'white',
    shadow = 'shadow',
    noBorder = 'noBorder'
}

interface ButtonProps {
    children?: ReactNode | string;
    handler: () => void;
    large?: boolean;
    styles?: ButtonStyles[]; // Используйте тип ButtonStyleType для стилей
    disabled?: boolean;
}

const Button = ({ children, handler, styles = [], large = false, disabled = false }: ButtonProps) => {
    const styleClasses: Record<string, boolean> = {};
    styles.forEach((style) => {
        styleClasses[classes[style]] = true;
    });

    return (
        <button className={cx({ [classes.button]: true, [classes.large]: large, ...styleClasses })} onClick={disabled ? () => null : handler}>
            {children}
        </button>
    );
}

export default Button;
