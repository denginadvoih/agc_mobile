import {PresentScreen} from "types/global";
import ReviewsScreen from "./ReviewsScreen/ReviewsScreen";
import React from "react";

export const presentScreens: PresentScreen[] = [
    {
        id: 1,
        title: 'Новый формат обслуживания автомобиля',
        description: 'Сравните цены и уровень сервиса. Почувствуйте разницу!',
        image: require('./images/image--1.jpg')
    },
    {
        id: 2,
        title: 'Бизнес класс по доступным ценам',
        description: 'Возьмем с любовью всю заботу о вашем автомобиле на себя',
        image: require('./images/image--2.jpg')
    },
    {
        id: 3,
        title: 'Специально для леди',
        description: 'Сервис в деталях',
        image: require('./images/image--3.jpg')
    },
    {
        id: 4,
        title: 'Открой для себя Мир будущего',
        description: 'Доставляем удовольствие от обслуживания автомобиля',
        image: require('./images/image--4.jpg')
    },
    {
        id: 5,
        title: 'Меняем представление об автосервисе',
        description: 'Сервис может быть приятным',
        image: require('./images/image--5.jpg')
    },
    {
        id: 6,
        title: 'Продлеваем молодость вашему автомобилю',
        description: 'Плановое обслуживание автомобилей с пробегом',
        image: require('./images/image--6.jpg')
    },
    {
        id: 7,
        title: '',
        description: '',
        theme: 'white',
        image: require('./images/image--7.jpg'),
        component: <ReviewsScreen/>
    }
];