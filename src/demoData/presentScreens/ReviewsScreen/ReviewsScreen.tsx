import React from 'react';
import classes from "./reviewsscreen.module.scss";

const ReviewsScreen = () => {
    return (
        <a href="https://www.vl.ru/avtogarant#comments" className={classes.text}>
            <div className={classes.row1}>На основе рейтинга пользователей VL.RU</div>
            <div className={classes.row2}>по итогу апреля 2024 года</div>
        </a>
    );
};

export default ReviewsScreen;