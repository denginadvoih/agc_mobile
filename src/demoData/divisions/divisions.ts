import {DivisionType} from "../../screens/Home/Divisions/Division/Division";

export const divisions: DivisionType[] = [
    {
        id: 1,
        title: 'современная Станция экспресс услуг',
        description: 'Мы помогаем гостю предупредить дорогостоящий ремонт через своевременное обслуживание',
        image: require('./image1.jpg'),
        url: 'http://all.autogarantcity.ru'
    },
    {
        id: 2,
        title: 'центр выгодного страхования',
        description: 'Мы помогаем гостю эффективно управлять рисками на высоком уровне сервиса',
        image: require('./image2.jpg'),
        hidden: true
    },
    {
        id: 3,
        title: 'Центр автомобильной безопасности',
        description: 'Мы помогаем гостю сделать эффективный выбор в охране автомобиля',
        image: require('./image3.jpg')
    },
];