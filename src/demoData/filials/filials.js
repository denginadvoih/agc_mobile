import {SHILKINSKAYA_LOCATION, SNEGOVAIA_LOCATION} from "../../utils/constants";

export const filials = {
    shilkinskaya: {
        photo: [require('./sh__mainPhoto.jpg'), require('./sh__ph2.jpg')],
        map: require('./sh__map.jpg'),
        yaMap: {defaultState: {center: [43.117701, 131.931212], zoom: 18}, width: '100%', height: '100%'},
        parking: [
            require('./sh__parking.jpg'),
            require('./sh__parking2.jpg')
        ],
        routeVideos: [
            {
                title: 'Как проехать в АвтоГарантСити с Гайдамака',
                id: 'CliDkrqTzzw',
                preview: require('./video_preview_gaidamak.jpg')
            },
            {
                title: 'Как проехать в АвтоГарантСити с 3-ей рабочей',
                id: 'AJot09duHmA',
                preview: require('./video_preview_3rab.jpg')
            },
            {
                title: 'Как проехать в АвтоГарантСити с фуникулёра',
                id: 'h1sIZ2Ljxnz4',
                preview: require('./video_preview_funikuler.jpg')
            },
            {
                title: 'Как проехать в АвтоГарантСити с тц "Славянский',
                id: 'hkM_oAVFmS8',
                preview: require('./video_preview_slav.jpg')            }
        ],
        location: SHILKINSKAYA_LOCATION
    },
    snegovaia: {
        photo: [require('./sn__mainPhoto.jpg')],
        map: require('./sn__map.jpg'),
        yaMap: {defaultState: {center: [43.136562, 131.927742], zoom: 18}, width: '100%', height: '100%'},
        parking: [require('./sh__parking.jpg')],
        routeVideos: [

        ],
        location: SNEGOVAIA_LOCATION
    }
};
