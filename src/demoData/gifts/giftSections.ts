export const giftSections = [
    {
        id: 1,
        title: 'Подарки новым гостям ',
        image: require('./image2.jpg')
    },
    {
        id: 2,
        title: 'Сезон «Зима 2023»',
        image: require('./image1.jpg')
    },
    {
        id: 3,
        title: 'Розыгрыши',
        image: require('./image3.jpg')
    }
];