export const presentServices = [
    {
        id: 1,
        angaraId: 1,
        title: 'Профессиональная замена масла в двигателе',
        description: '',
        image: require('./service1.png'),
        url: 'https://all.autogarantcity.ru/'
    },
    {
        id: 2,
        angaraId: 2,
        title: 'Профессиональная диагностика ходовой части',
        description: '',
        image: require('./service2.png')
    },
    {
        id: 3,
        angaraId: 3,
        title: 'Экспертная консультация по охране',
        description: '',
        image: require('./service3.png')
    },
];