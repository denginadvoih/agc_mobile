import React from "react";
import OrtusButton from "./OrtusButton";

export interface OrtusScreens {
    title?: string;
    description?: string | JSX.Element;
    extraItem?: React.ReactElement;
    image?: string;
};
export const ortusScreens: OrtusScreens[] = [
    {
        title: 'История обслуживания',
        description: <div><p>Лёгкое заполнение истории обслуживания вашего автомобиля</p>
            <p>Автоматическое заполнение истории обслуживания, при получении услуг в «АвтоГарантСити»</p>
            <p>Хранение фотографий чеков и заказ-нарядов для подтверждения проведённых работ</p></div>,
        image: require('./images/screen__1.png')
    },
    {
        title: 'Стоимость владения автомобилем',
        description: <div><p>Автоматическое сохранение стоимости обслуживания вашего автомобиля, при получении услуг в
            «АвтоГарантСити»</p>
            <p>Ведение всех расходов на автомобиль при помощи удобного интерфейса для ручного занесения данных</p>
            <p>Статистика расходов на авто по категории и периоду времени</p></div>,
        image: require('./images/screen__2.png')
    },
    {
        title: 'Напоминания об обслуживании',
        description: <div><p>Расчёт даты будущей замены масла (и других жидкостей) в соответствии с актуальным пробегом
            автомобиля</p>
            <p>Напоминания о сезонной смене шин (переобувке автомобиля)</p>
            <p>Предложение записаться на услугу, когда подходит время обслуживания</p></div>,
        image: require('./images/screen__3.png')
    },
    {
        title: 'Онлайн-запись за одну минуту',
        description: <div><p>Online-запись в «АвтоГарантСити» с автоматической подгрузкой данных о доступном для записи времени с гарантированным бронированием</p>
            <p>Напоминание о времени визита в день записи</p></div>,
        extraItem: <OrtusButton/>,
        image: require('./images/screen__4.png')
    },
]