import React from 'react';
import Button from "../../components/Button";
import {ButtonStyles} from "../../components/Button/Button";

const OrtusButton = () => {
    const styles: ButtonStyles[] = [ButtonStyles.whiteBorder, ButtonStyles.fullWidth];
    return (
        <div>
            <Button children="Скачать Ortus" handler={() => {
            }} styles={styles}/>
        </div>
    );
};

export default OrtusButton;