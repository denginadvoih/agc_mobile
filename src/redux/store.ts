import { configureStore } from '@reduxjs/toolkit';
import dataReducer from './slices/dataSlice';
import hangarReducer from './slices/hangarSlice';

const store = configureStore({
    reducer: {
        hangar: hangarReducer,
        data: dataReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
