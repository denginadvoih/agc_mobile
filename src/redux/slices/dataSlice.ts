import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import { api, API_URL } from '../../utils';
import { FETCH_RESULT } from '../../utils/constants';

// Async Thunks
export const sendMail = createAsyncThunk(
    'data/sendMail',
    async (data: any, { dispatch }) => {
        const response = await api('https://www.autogarantcity.ru/mobile/ajax/index.php?method=sendMail', {
            method: 'POST',
            body: JSON.stringify(data),
        }, dispatch);
        return response;
    }
);

export const fetchServices = createAsyncThunk(
    'data/fetchServices',
    async (_, { dispatch }) => {
        const response = await api('https://api.ortus.ru/v1/widgets/13/services?expand=serviceOptions,serviceTemplate,serviceOptionTemplates,serviceOption.service,serviceOptionReference,reference,values', {
            method: 'GET'
        }, dispatch);
        return response;
    }
);

export const fetchCatalogues = createAsyncThunk(
    'data/fetchCatalogues',
    async (_, { dispatch }) => {
        const response = await api('https://api.ortus.ru/v1/widgets/13/catalogues', {
            method: 'GET'
        }, dispatch);
        return response;
    }
);

export const fetchReviews = createAsyncThunk(
    'data/fetchReviews',
    async (_, { dispatch }) => {
        const response = await api('https://www.autogarantcity.ru/mobile/ajax/index.php?method=reviews', {
            method: 'GET'
        }, dispatch);
        return response;
    }
);

export const fetchServiceCards = createAsyncThunk(
    'data/fetchServiceCards',
    async (_, { dispatch }) => {
        const response = await api('https://www.autogarantcity.ru/mobile/ajax/index.php?method=service_cards', {
            method: 'GET'
        }, dispatch);
        return response;
    }
);

// Helper function to flatten catalogues
function flatCatalogues(catalogues: any[]) {
    const result: any[] = [];
    const springItems = (result: any[], brunch: any[]) => {
        brunch.forEach(c => {
            result.push({
                id: c.id,
                name: c.name,
                parentCatalogueId: get(c, 'parentCatalogue.id', null),
                serviceTemplates: c.serviceTemplates.map((st: any) => ({
                    ...st,
                    services: st.services.filter((s: any) => s.isHidden !== true)
                })),
                sort: c.sort
            });
            springItems(result, c.childrenCatalogues || []);
        });
    }
    springItems(result, catalogues);
    return result;
}

// Slice
interface Catalogue {
    id: number;
    name: string;
    parentCatalogueId: number | null;
    serviceTemplates: ServiceTemplate[];
    sort: number;
}

interface ServiceTemplate {
    id: number;
    name: string;
    services: Service[];
}

interface Service {
    id: number;
    name: string;
    isHidden: boolean;
}

interface Review {
    id: number;
    origin: string;
    image: string;
}

interface FetchingStatus {
    [key: string]: boolean;
}

interface DataState {
    catalogues: Catalogue[];
    services: Record<string, any>;
    serviceCards: any; // Типизация для serviceCards зависит от структуры данных, которую возвращает ваш API
    fetching: FetchingStatus;
    reviews: Review[] | null;
    mailIsSent: FETCH_RESULT;
}

// Начальное состояние
const initialState: DataState = {
    catalogues: [],
    services: {},
    serviceCards: null,
    fetching: {},
    reviews: null,
    mailIsSent: FETCH_RESULT.idle,
};

// Слайс
const dataSlice = createSlice({
    name: 'data',
    initialState,
    reducers: {
        sendMailReset(state) {
            state.mailIsSent = FETCH_RESULT.idle;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(sendMail.pending, (state) => {
                state.fetching['sendMail'] = true;
            })
            .addCase(sendMail.fulfilled, (state) => {
                state.mailIsSent = FETCH_RESULT.success;
                state.fetching['sendMail'] = false;
            })
            .addCase(sendMail.rejected, (state) => {
                state.fetching['sendMail'] = false;
            })
            .addCase(fetchServices.pending, (state) => {
                state.fetching['fetchServices'] = true;
            })
            .addCase(fetchServices.fulfilled, (state, action) => {
                state.services = action.payload;
                state.fetching['fetchServices'] = false;
            })
            .addCase(fetchServices.rejected, (state) => {
                state.fetching['fetchServices'] = false;
            })
            .addCase(fetchCatalogues.pending, (state) => {
                state.fetching['fetchCatalogues'] = true;
            })
            .addCase(fetchCatalogues.fulfilled, (state, action) => {
                state.catalogues = flatCatalogues(action.payload.items);
                state.fetching['fetchCatalogues'] = false;
            })
            .addCase(fetchCatalogues.rejected, (state) => {
                state.fetching['fetchCatalogues'] = false;
            })
            .addCase(fetchReviews.pending, (state) => {
                state.fetching['fetchReviews'] = true;
            })
            .addCase(fetchReviews.fulfilled, (state, action) => {
                state.reviews = action.payload.map((r: any) => ({
                    id: parseInt(r.id),
                    origin: r.section.name,
                    image: API_URL + r.detail_picture
                }));
                state.fetching['fetchReviews'] = false;
            })
            .addCase(fetchReviews.rejected, (state) => {
                state.fetching['fetchReviews'] = false;
            })
            .addCase(fetchServiceCards.pending, (state) => {
                state.fetching['fetchServiceCards'] = true;
            })
            .addCase(fetchServiceCards.fulfilled, (state, action) => {
                state.serviceCards = action.payload;
                state.fetching['fetchServiceCards'] = false;
            })
            .addCase(fetchServiceCards.rejected, (state) => {
                state.fetching['fetchServiceCards'] = false;
            });
    },
});

export const { sendMailReset } = dataSlice.actions;
export default dataSlice.reducer;
