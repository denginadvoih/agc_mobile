import { createSlice } from '@reduxjs/toolkit';

// Slice
const hangarSlice = createSlice({
    name: 'hangar',
    initialState: {
        startStatus: true,
    },
    reducers: {
        changeStartStatus(state, action) {
            state.startStatus = action.payload;
        },
        setAjaxError(state, action) {
            state.startStatus = action.payload;
        }
    }
});

export const { changeStartStatus, setAjaxError } = hangarSlice.actions;
export default hangarSlice.reducer;
