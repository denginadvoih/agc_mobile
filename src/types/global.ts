import React from "react";

export enum Screens { main = 'main', contacts = 'contacts', acquaint = 'acquaint' }
export interface ReviewDto {
    id: number;
    origin: string;
    image: string;
}
export interface PresentScreen {
    id: number;
    title: string;
    description: string;
    image: string;
    theme?: 'white';
    component?: React.ReactNode;
}

export interface ServiceCard {
    id: number;
    title: string;
    image: string;
    gift: Record<string, string>;
    url: string;
}
